﻿#version 150

////////////////////////////////////////////////////////////////////////////////【定数】

const float Pi = 3.141592653589793;

const float Pi2 = Pi * 2.0;

const float P2i = Pi / 2.0;

////////////////////////////////////////////////////////////////////////////////【ルーチン】

vec2 VecToSky( vec4 Vector_ )
{
    vec2 Result;

    Result.x = ( Pi - atan( -Vector_.x, -Vector_.z ) ) / Pi2;
    Result.y =        acos(  Vector_.y             )   / Pi ;

    return Result;
}

////////////////////////////////////////////////////////////////////////////////【共通定数】

layout(std140) uniform TCameraDat
{
  layout(row_major) mat4 Proj;
  layout(row_major) mat4 Move;
}
_Camera;

layout(std140) uniform TShaperDat
{
  layout(row_major) mat4 Move;
}
_Shaper;

uniform sampler2D _Imager1;
uniform sampler2D _Imager2;

////////////////////////////////////////////////////////////////////////////////【入出力】

in TSendVF
{
  vec4 Pos;
  vec4 Nor;
  vec2 Tex;
  vec4 Tan;
  vec4 Bin;
  /*float Dif;       */
}
_Sender;

//------------------------------------------------------------------------------

out vec4 _Frag_Col;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

void main()
{
  vec4 C = _Camera.Move * vec4( 0, 0, 0, 1 );
  vec4 V = normalize( _Sender.Pos - C );
  vec3 N = normalize( _Sender.Nor.xyz );                                    //表面法線（グローバル）
  vec3 T = normalize( _Sender.Tan.xyz );                                    //表面接線（グローバル）
  vec3 B = normalize( _Sender.Bin.xyz );

  vec4 NP = texture(_Imager2, _Sender.Tex);

  vec3 _N = normalize( ( NP.x - 0.5 ) * 2 * T
                 + ( NP.y - 0.5 ) * 2 * B
                 +   NP.z             * N );

  vec4 R = reflect( V, vec4(_N, 1.0) );

  vec4 lightpos = vec4(0.0, -5.0, 0.0, 1.0);
  vec3 light = normalize(lightpos.xyz - _Sender.Pos.xyz);

  float dif = max(dot(-light, _N), 0.0);

  _Frag_Col = dif * texture(_Imager1, VecToSky(R) );
}

//##############################################################################
